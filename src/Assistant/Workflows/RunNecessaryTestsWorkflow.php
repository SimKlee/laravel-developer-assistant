<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Git\Git;
use SimKlee\LaravelDeveloperAssistant\Phpunit\Extensions\PhpunitJsonResultPrinterExtension;
use SimKlee\LaravelDeveloperAssistant\Phpunit\Phpunit;
use SimKlee\LaravelDeveloperAssistant\Phpunit\TestSelector;

use function Laravel\Prompts\info;
use function Laravel\Prompts\spin;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\line;
use function SimKlee\LaravelDeveloperAssistant\Functions\Prompts\pause;

class RunNecessaryTestsWorkflow extends AbstractWorkflow
{
    public function info(): string
    {
        return 'Workflow: Run necessary tests';
    }

    public function process(): bool
    {
        $status   = (new Git())->status();
        $selector = new TestSelector($status, base_path('build/coverage/xml'));

        if ($selector->tests->count() > 0) {
            info(sprintf('Changed files (%s):', $status->all()->count()));
            $status->all()->each(fn(string $file) => line(' - ' . $file));
            line('');
            info(sprintf('Necessary tests (%s):', $selector->tests->count()));
            $selector->tests->each(fn(string $file) => line(' - ' . $file));
            line('');

            (new Phpunit())->throughSail()->configuration('phpunit.json.xml')->run();
            pause();
            return true;

            spin(
                callback: fn() => (new Phpunit())->throughSail()->configuration('phpunit.json.xml')->run(),
                message : $this->info()
            );
            pause();
        }

        return true;
    }
}