<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Git\GitStatusResult;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\text;
use function Laravel\Prompts\warning;
use function Laravel\Prompts\info;

class CommitChangesWorkflow extends AbstractGitWorkflow
{
    public function __construct(private ?JiraIssueDtoInterface $issue = null)
    {
        parent::__construct();
    }

    public function info(): string
    {
        return 'Workflow: Commit changes';
    }

    public function process(): bool
    {
        $changes = $this->git->status();

        if ($changes->clean()) {
            info('Nothing to commit.');
            return true;
        }

        return $this->commit($changes);
    }

    private function commit(GitStatusResult $changes): bool
    {
        warning(sprintf('You have %s changes.', $changes->all()->count()));
        if (confirm('Commit changes?') === false) {
            return false;
        }

        while ($changes->dirty()) {
            $options = $changes->all()
                               ->mapWithKeys(fn(string $file) => [$file => 'FILE: ' . $file])
                               ->prepend('ACTION: Commit all', 'all')
                               ->prepend('ACTION: Skip', 'skip');

            $files = collect(multiselect(
                label   : 'Select:',
                options : $options,
                scroll  : 20,
                required: true
            ));

            if ($files->contains('skip')) {
                break;
            }

            if ($files->contains('all')) {
                $files = $changes->all();
            }

            if (count($files) > 0) {
                $default = '';
                if ($this->issue instanceof JiraIssueDtoInterface) {
                    $default = sprintf('%s: %s', $this->issue->key, $this->issue->summary);
                }

                $this->git->add($files)
                          ->commit(text(label: 'Commit message', default: $default, required: true));

                $changes = $this->git->status();
            }
        }

        $this->git->pull()
                  ->push();

        (new SolveConflictsWorkflow())->process();

        return true;
    }
}