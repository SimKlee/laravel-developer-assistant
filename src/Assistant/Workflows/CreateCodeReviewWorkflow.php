<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;
use function Laravel\Prompts\intro;

class CreateCodeReviewWorkflow extends AbstractWorkflow
{
    public function __construct(private JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Create code review';
    }

    public function process(): bool
    {
        intro($this->info());
        (new CommitChangesWorkflow($this->issue))->process();

        return true;
    }
}