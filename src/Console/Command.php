<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Console;

use Illuminate\Contracts\Process\ProcessResult;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Process;

class Command
{
    public const OPTION_FILTER_EMPTY_OUTPUT_LINES = 'filter_empty_output_lines';
    public const OPTION_TRIM_OUTPUT_LINES         = 'trim_output_lines';
    public const OPTION_TIMEOUT                   = 'timeout';

    private readonly Process $process;
    private ?ProcessResult   $processResult = null;
    private array            $options       = [
        self::OPTION_FILTER_EMPTY_OUTPUT_LINES => true,
        self::OPTION_TRIM_OUTPUT_LINES         => false,
        self::OPTION_TIMEOUT                   => 60,
    ];

    public function __construct(private readonly string  $command,
                                private readonly ?string $workingDirectory = null,
                                array                    $options = [])
    {
        $this->process = new Process();

        if ($this->workingDirectory) {
            $this->process::path($this->workingDirectory);
        }

        $this->options = array_merge($this->options, $options);
    }

    public function execute(): void
    {
        $this->processResult = $this->process::timeout($this->options[self::OPTION_TIMEOUT])
                                             ->run($this->command);
    }

    public function successful(): bool
    {
        return $this->processResult?->successful() ?? false;
    }

    public function failed(): bool
    {
        return $this->processResult?->failed() ?? true;
    }

    /**
     * @return Collection|string[]
     */
    public function output(): Collection
    {
        if ($this->processResult) {
            $output = collect(explode(PHP_EOL, $this->processResult->output()));

            if ($this->options[self::OPTION_TRIM_OUTPUT_LINES] ?? false) {
                $output = $output->map(fn(string $line) => trim($line));
            }

            if ($this->options[self::OPTION_FILTER_EMPTY_OUTPUT_LINES] ?? true) {
                $output = $output->reject(fn(string $line) => empty($line));
            }

            return $output;
        }

        return new Collection();
    }
}