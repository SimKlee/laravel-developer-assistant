<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit\Extensions;

use PHPUnit\Runner\Extension\Extension;
use PHPUnit\Runner\Extension\Facade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration;
use PHPUnit\TextUI\Output\DefaultPrinter;

/**
 * @see https://github.com/TomasVotruba/phpunit-json-result
 * @see https://tomasvotruba.com/blog/get-json-output-for-phpunit-10
 */
class PhpunitJsonResultPrinterExtension implements Extension
{
    private PhpunitJsonResultPrinter $printer;

    public function __construct()
    {
        $this->printer = new PhpunitJsonResultPrinter(DefaultPrinter::standardOutput());
    }

    public function bootstrap(Configuration $configuration, Facade $facade, ParameterCollection $parameters): void
    {
        if ($configuration->noOutput()) {
            return;
        }

        $facade->replaceOutput();
        $facade->registerSubscribers(
            new PhpunitJsonResultPrinterFinishedSubscriber($this->printer),
        );
    }
}