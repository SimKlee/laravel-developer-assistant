<?php
/**
 * @var \SimKlee\LaravelDeveloperAssistant\Phploc\PhplocResult $phploc
 * @var \SimKlee\LaravelDeveloperAssistant\Git\GitStatusResult $gitStatus
 */
?>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            Assistant
        </h2>
    </x-slot>

    <div class="m-4 border rounded-lg bg-white p-4 w-1/3">
        <h1 class="text-xl font-bold mb-2 border-b">Code Metrics</h1>
        <div class="flex justify-between">
            <div>Lines of Code</div>
            <div>{{ number_format($phploc->loc, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Files</div>
            <div>{{ number_format($phploc->files, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Classes</div>
            <div>{{ number_format($phploc->classes, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Methods</div>
            <div>{{ number_format($phploc->methods, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Interfaces</div>
            <div>{{ number_format($phploc->interfaces, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Traits</div>
            <div>{{ number_format($phploc->traits, 0) }}</div>
        </div>

        <h1 class="text-xl font-bold mb-2 border-b">Laravel Metrics</h1>
        <div class="flex justify-between">
            <div>Command</div>
            <div>{{ number_format($laravel->command, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Controller</div>
            <div>{{ number_format($laravel->controller, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Middleware</div>
            <div>{{ number_format($laravel->middleware, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Model</div>
            <div>{{ number_format($laravel->model, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Action</div>
            <div>{{ number_format($laravel->action, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Job</div>
            <div>{{ number_format($laravel->job, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Event</div>
            <div>{{ number_format($laravel->event, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Listener</div>
            <div>{{ number_format($laravel->listener, 0) }}</div>
        </div>
        <div class="flex justify-between">
            <div>Blade Views</div>
            <div>{{ number_format($laravel->blade, 0) }}</div>
        </div>
    </div>

    <div class="m-4 border rounded-lg bg-white p-4 w-1/3">
        <h1 class="text-xl font-bold mb-2">Branch: {{ $branch }}</h1>
        <h1 class="text-xl font-bold mb-2">Changed files:</h1>
        <ul class="px-2">
        @foreach($gitStatus->all() as $file)
            <li class="text-sm">{{ $file }}</li>
        @endforeach
        </ul>
    </div>

</x-app-layout>