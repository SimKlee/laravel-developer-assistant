<?php

namespace SimKlee\LaravelDeveloperAssistant\Prompts;

use Laravel\Prompts\ConfirmPrompt;
use SimKlee\LaravelDeveloperAssistant\Prompts\Renderer\PausePromptRenderer;

class PausePrompt extends ConfirmPrompt
{
    public string          $message = 'Press any key to continue ...';
    protected static array $themes  = [
        'default' => [
            PausePrompt::class => PausePromptRenderer::class,
        ],
    ];

    public function __construct($label = 'Paused')
    {
        parent::__construct($label);
        $this->on('key', fn($key) => $this->submit());
    }

    public function value(): bool
    {
        return true;
    }

    public function label(): string
    {
        return '';
    }
}
