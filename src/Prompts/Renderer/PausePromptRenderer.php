<?php

namespace SimKlee\LaravelDeveloperAssistant\Prompts\Renderer;

use Laravel\Prompts\Themes\Default\Concerns\DrawsBoxes;
use Laravel\Prompts\Themes\Default\Renderer;
use SimKlee\LaravelDeveloperAssistant\Prompts\PausePrompt;

class PausePromptRenderer extends Renderer
{
    use DrawsBoxes;

    public function __invoke(PausePrompt $prompt): string
    {
        return match ($prompt->state) {
            'submit' => $this
                ->box(
                    $this->dim($this->truncate($prompt->label, $prompt->terminal()->cols() - 6)),
                    $this->truncate($prompt->label(), $prompt->terminal()->cols() - 6)
                ),

            default => $this
                ->box(
                    $this->cyan($this->truncate($prompt->label, $prompt->terminal()->cols() - 6)),
                    $this->renderOptions($prompt),
                )
                ->when(
                    $prompt->hint,
                    fn () => $this->hint($prompt->hint),
                    fn () => $this->newLine()
                ),
        };
    }

    protected function renderOptions(PausePrompt $prompt): string
    {
        return $prompt->message;
    }
}
