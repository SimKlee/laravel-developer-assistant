<?php

use Illuminate\Support\Facades\Route;
use SimKlee\LaravelDeveloperAssistant\Git\Git;
use SimKlee\LaravelDeveloperAssistant\Phploc\LaravelMetrics;
use SimKlee\LaravelDeveloperAssistant\Phploc\PhplocResult;

Route::get('/assistant', function () {
    $git = new Git();
    return view('laravel-developer-assistant::dashboard')
        ->with('phploc', new PhplocResult(base_path('build/phploc.json')))
        ->with('gitStatus', $git->status())
        ->with('laravel', new LaravelMetrics())
        ->with('branch', $git->currentBranch());
});