<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Functions\Base;

use Closure;

function when(bool $condition, Closure $then, ?Closure $else = null): void
{
    if ($condition) {
        $then();
    } elseif (!is_null($else)) {
        $else();
    }
}