<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit\Extensions;

use JsonException;
use PHPUnit\Event\Code\TestMethod;
use PHPUnit\Event\TestRunner\Finished;
use PHPUnit\Event\TestRunner\FinishedSubscriber;
use PHPUnit\Metadata\DataProvider;
use PHPUnit\TestRunner\TestResult\Facade;
use PHPUnit\TestRunner\TestResult\TestResult;

class PhpunitJsonResultPrinterFinishedSubscriber implements FinishedSubscriber
{
    public function __construct(private readonly PhpunitJsonResultPrinter $printer)
    {
    }

    /**
     * @throws JsonException
     */
    public function notify(Finished $event): void
    {
        $testResult = Facade::result();

        $json = [
            'counts' => [
                'tests'        => $testResult->numberOfTestsRun(),
                'failed'       => $testResult->numberOfTestFailedEvents(),
                'assertions'   => $testResult->numberOfAssertions(),
                'errors'       => $testResult->numberOfTestErroredEvents(),
                'warnings'     => $testResult->numberOfWarnings(),
                'deprecations' => $testResult->numberOfDeprecations(),
                'notices'      => $testResult->numberOfNotices(),
                'success'      => $testResult->numberOfTestsRun() - $testResult->numberOfTestErroredEvents(),
                'incomplete'   => $testResult->numberOfTestMarkedIncompleteEvents(),
                'risky'        => $testResult->numberOfTestsWithTestConsideredRiskyEvents(),
                'skipped'      => $testResult->numberOfTestSuiteSkippedEvents() + $testResult->numberOfTestSkippedEvents(),
            ],
            'failed' => $this->getFailedTests($testResult),
        ];

        $this->printer->writeln(json_encode($json, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR));
    }

    private function getFailedTests(TestResult $testResult): array
    {
        $data = [];

        foreach ($testResult->testFailedEvents() as $testFailedEvent) {
            $testMethod = $testFailedEvent->test();

            /** @var TestMethod $testMethod */
            $failedEventData = [
                'test_class'      => $testMethod->className(),
                'test_method'     => $testMethod->methodName(),
                'message'         => $testFailedEvent->throwable()->message(),
                'exception_class' => $testFailedEvent->throwable()->className(),
                'line'            => $this->lineNumber($testFailedEvent->throwable()->stackTrace()),
            ];

            if ($testMethod->testData()->hasDataFromDataProvider()) {
                $failedEventData['data_provider'] = $this->dataProvider($testMethod);
            }

            $data[] = $failedEventData;
        }

        return $data;
    }

    private function dataProvider(TestMethod $testMethod): array
    {
        $dataFromDataProvider = $testMethod->testData()->dataFromDataProvider();

        $dataProviderData = [
            'key'  => $dataFromDataProvider->dataSetName(),
            'data' => $dataFromDataProvider->data(),
        ];

        foreach ($testMethod->metadata() as $metadata) {
            if ($metadata instanceof DataProvider) {
                $dataProviderData['provider_method'] = $metadata->methodName();
            }
        }

        return $dataProviderData;
    }

    private function lineNumber(string $stackTrace): int
    {
        preg_match('#:(?<line>\d+)$#', $stackTrace, $matches);

        return (int) $matches['line'];
    }
}