<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Pint\Pint;
use function Laravel\Prompts\spin;

class PintChangedFilesWorkflow extends AbstractWorkflow
{
    public function info(): string
    {
        return 'Workflow: Checks the code style for changed files';
    }

    public function process(): bool
    {
        spin(
            callback: fn() => (new Pint())->dirty()->run(),
            message : $this->info()
        );

        return true;
    }
}