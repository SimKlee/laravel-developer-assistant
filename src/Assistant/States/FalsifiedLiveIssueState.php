<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class FalsifiedLiveIssueState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['Live'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            '"Verified[Dropdown]" = No',
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('assignee = %s', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking if any issues assigned to you with status "Live" are falsified.';
    }
}