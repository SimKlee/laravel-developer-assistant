<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Git;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Console\Command;

class Git
{
    private Collection $output;
    private ?Command   $command;

    public function __construct()
    {
        $this->output = new Collection();
    }

    public function status(): GitStatusResult
    {
        $this->execute(command: 'git status --porcelain', trim: false);

        return new GitStatusResult($this->output);
    }

    public function pull(): self
    {
        return $this->execute(command: 'git pull');
    }

    public function push(): self
    {
        return $this->execute(command: 'git push');
    }

    public function fetch(): self
    {
        return $this->execute(command: 'git fetch');
    }

    public function createBranch(string $name): bool
    {
        $this->execute(command: 'git checkout -b ' . $name);

        if ($this->command->successful()) {
            $this->execute('push -u origin ' . $name);

            return $this->command->successful();
        }

        return false;
    }

    public function deleteBranch(string $name): bool
    {
        $this->execute(command: 'git branch -d ' . $name);

        if ($this->command->successful()) {
            $this->execute('push origin -d ' . $name);

            return $this->command->successful();
        }

        return false;
    }

    public function currentBranch(): string|null
    {
        $this->execute(command: 'git branch --show-current');

        return $this->output->first();
    }

    public function isDevelopBranch(): bool
    {
        return $this->currentBranch() === config('laravel-developer-assistant.git.branches.develop');
    }

    public function isMainBranch(): bool
    {
        return $this->currentBranch() === config('laravel-developer-assistant.git.branches.main');
    }

    /**
     * @return Collection|string[]
     */
    public function branches(): Collection
    {
        $this->execute(command: 'git branch --no-color');

        $this->output = $this->output
            ->map(fn(string $line) => trim(str_replace('*', '', $line)));

        return $this->output;
    }

    public function checkout(string $branch): self
    {
        return $this->execute(command: 'git checkout ' . $branch);
    }

    public function commit(string $message): self
    {
        return $this->execute(command: sprintf('git commit -m "%s"', $message));
    }

    /**
     * @param string|string[]|Collection $pathspec
     */
    public function add(string|array|Collection $pathspec = '.'): self
    {
        if ($pathspec instanceof Collection) {
            $pathspec = $pathspec->toArray();
        }

        if (is_array($pathspec)) {
            $pathspec = implode(' ', $pathspec);
        }

        return $this->execute(command: sprintf('git add %s', $pathspec));
    }

    public function notPushed(): bool
    {
        $this->execute('git diff --stat origin/' . $this->currentBranch());

        return $this->output->count() > 0;
    }

    public function conflicts(): Collection
    {
        $this->execute('git grep -l "<<<<<<< HEAD"');

        return $this->output;
    }

    private function execute(string $command, bool $trim = true): self
    {
        $this->command = new Command(
            command: $command,
            options: [
                Command::OPTION_FILTER_EMPTY_OUTPUT_LINES => true,
                Command::OPTION_TRIM_OUTPUT_LINES         => $trim,
            ]
        );
        $this->command->execute();
        $this->output = $this->command->output();

        return $this;
    }
}