<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\NextJiraIssueWorkflow;
use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssues;

abstract class AbstractJiraBasedState extends AbstractState implements JiraIssuesInterface
{
    protected Collection $issues;
    protected array      $issueStatus = [];
    protected array      $issueTypes  = [];

    public function __construct(array $issueStatus = null, array $issueTypes = null)
    {
        parent::__construct();
        $this->issues = new Collection();

        if (!is_null($issueStatus)) {
            $this->issueStatus = $issueStatus;
        }

        if (!is_null($issueTypes)) {
            $this->issueTypes = $issueTypes;
        }
    }

    public function check(): bool
    {
        $jira = new JiraIssues();

        $conditions = $this->conditions();
        if (count($this->issueStatus) > 0) {
            $conditions .= sprintf(' AND status IN (%s)', implode(', ', $this->issueStatus));
        }
        if (count($this->issueTypes) > 0) {
            $conditions .= sprintf(' AND type IN (%s)', implode(', ', $this->issueTypes));
        }

        $jql          = $conditions . ' ' . $this->orderBy();
        $this->issues = $jira->send($jql);

        return $this->issues->count() > 0;
    }

    public function message(): string
    {
        return $this->issues->count() > 0
            ? sprintf('Found %s issues!', $this->issues->count())
            : 'Found no issues.';
    }

    public function orderBy(): string
    {
        return 'ORDER BY rank ASC';
    }

    public function issues(): Collection
    {
        return $this->issues;
    }

    public function process(): bool
    {
        $nextJiraWorkflow = new NextJiraIssueWorkflow($this->issues);
        info($nextJiraWorkflow->info());

        return $nextJiraWorkflow->process();
    }

    abstract public function conditions(): string;
}