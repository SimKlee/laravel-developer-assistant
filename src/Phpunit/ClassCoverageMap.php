<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use SplFileInfo;

class ClassCoverageMap
{
    public Collection $map;
    public ?Carbon    $created = null;

    public function __construct(private readonly string $pathToXmlReport)
    {
        $this->map = new Collection();
        $this->analyseXmlReport();
        $this->created = self::indexCreation($this->pathToXmlReport);
    }

    public static function indexCreation(string $pathToXmlReport): Carbon|null
    {
        $indexFile = $pathToXmlReport . '/index.xml';
        if (File::exists($indexFile) === false) {
            return null;
        }

        $index = json_decode(json_encode(simplexml_load_file($indexFile)), true);

        return Carbon::parse($index['build']['@attributes']['time']);
    }

    private function analyseXmlReport(): void
    {
        $this->analyseDirectory($this->pathToXmlReport);
    }

    private function analyseDirectory(string $directory): void
    {
        collect(File::files($directory))
            ->each(function (SplFileInfo $fileInfo) {
                $coverage = new ClassCoverage($fileInfo->getPathname());
                if ($coverage->className(false)) {
                    $this->map->put($coverage->fileName(), $coverage);
                }
            });

        collect(File::directories($directory))
            ->each(fn(string $subDirectory) => $this->analyseDirectory($subDirectory));
    }
}