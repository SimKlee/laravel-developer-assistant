<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\intro;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\selectNextStatus;

class SubtaskWorkflow extends AbstractWorkflow
{
    public ?string $key = null;

    public function __construct(private JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Handle subtask';
    }

    public function process(): bool
    {
        if ($this->issue->type !== 'Sub-task') {
            return false;
        }

        intro($this->info());
        assignIssue(issue: $this->issue, confirm: true);
        selectNextStatus($this->issue);

        $this->issue = getIssue($this->issue->key);
        if ($this->issue->status === 'Done') {
            (new CommitChangesWorkflow($this->issue))->process();
        }

        return false;
    }
}