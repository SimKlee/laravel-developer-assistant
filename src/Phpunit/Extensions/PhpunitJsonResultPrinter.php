<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit\Extensions;

use PHPUnit\TextUI\Output\Printer;

final class PhpunitJsonResultPrinter
{
    public function __construct(private readonly Printer $phpunitPrinter) {
    }

    public function writeln(string $content): void
    {
        $this->phpunitPrinter->print($content . PHP_EOL);
    }
}