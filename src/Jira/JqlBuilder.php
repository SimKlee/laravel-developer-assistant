<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira;

class JqlBuilder
{
    public const PROPERTY_PROJECT = 'project';
    public const PROPERTY_STATUS  = 'status';
    public const PROPERTY_SPRINT  = 'Sprint';

    public static function factory(): JqlBuilder
    {
        return new self();
    }

    public function inOpenSprints(): self
    {
        return $this;
    }

    public function notInFutureSprints(): self
    {
        return $this;
    }

    public function assignee(string $user): self
    {
        return $this;
    }

    public function reporter(string $user): self
    {
        return $this;
    }

    public function where(string $filed, string $operator, mixed $value): self
    {
        return $this;
    }

    public function whereEqual(string $filed, string $operator, mixed $value): self
    {
        return $this;
    }

    public function whereNotEqual(string $filed, string $operator, mixed $value): self
    {
        return $this;
    }

    public function whereIn(string $filed, array $values): self
    {
        return $this;
    }

    public function whereNotIn(string $filed, array $values): self
    {
        return $this;
    }

    public function orderBy(): self
    {
        return $this;
    }

    public function jsonString(): string
    {
        return '';
    }
}