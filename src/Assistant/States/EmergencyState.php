<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class EmergencyState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['Open'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            'priority = "! APOCALYPSE !"',
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('(assignee IS EMPTY OR assignee = %s)', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking if new emergency issues are present.';
    }
}