<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Pint;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Console\Command;

class Pint
{
    private Collection $output;
    private Collection $options;
    private Collection $files;
    private bool       $sail = false;

    public function __construct(private readonly string $pathToPint = 'vendor/bin')
    {
        $this->output  = new Collection();
        $this->options = new Collection();
        $this->files   = new Collection();
    }

    public function test(): self
    {
        $this->options->put('--test', true);

        return $this;
    }

    public function dirty(): self
    {
        $this->options->put('--dirty', true);

        return $this;
    }

    public function files(Collection|array|string $files): self
    {
        if (is_string($files)) {
            $files = collect([$files]);
        }

        if (is_array($files)) {
            $files = collect($files);
        }

        if ($files instanceof Collection) {
            $files->each(fn(string $file) => $this->files->add($file));
        }

        return $this;
    }

    public function run(): void
    {
        $this->output = new Collection();
        $command      = new Command(
            command         : $this->buildCommand(),
            workingDirectory: base_path(),
            options         : [
                Command::OPTION_FILTER_EMPTY_OUTPUT_LINES => true,
                Command::OPTION_TRIM_OUTPUT_LINES         => true,
                Command::OPTION_TIMEOUT                   => 300,
            ]
        );
        $command->execute();
        $this->output = $command->output();
    }

    private function buildCommand(): string
    {
        $command = $this->pathToPint;
        $command .= $this->sail ? '/sail pint' : '/pint';
        if ($this->options->count() > 0) {
            $command .= ' ' . $this->options->map(
                    fn($value, string $option) => ($value === true) ? $option : sprintf('%s %s', $option, $value)
                )->implode(' ');
        }

        return $command;
    }

    public function output(): Collection
    {
        return $this->output;
    }
}