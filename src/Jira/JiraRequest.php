<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class JiraRequest
{
    public readonly string  $url;
    public readonly string  $user;
    private readonly string $token;

    public function __construct()
    {
        $this->url   = config('laravel-developer-assistant.jira.api_url');
        $this->user  = config('laravel-developer-assistant.jira.user_email');
        $this->token = config('laravel-developer-assistant.jira.token');
    }

    public function client(): PendingRequest
    {
        return Http::withHeaders([
            'Authorization' => $this->getBasicAuthentication(),
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ])->withOptions([
            'connection_timeout' => 5,
            'timeout'            => 10,
        ]);
    }

    private function getBasicAuthentication(): string
    {
        return sprintf('Basic %s', base64_encode(sprintf('%s:%s', $this->user, $this->token)));
    }
}