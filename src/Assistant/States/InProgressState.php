<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class InProgressState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['In Progress'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('assignee = %s', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking if any of your issues are in progress.';
    }
}