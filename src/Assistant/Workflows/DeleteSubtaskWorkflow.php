<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssue;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\text;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\clear;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\selectNextStatus;

class DeleteSubtaskWorkflow extends AbstractWorkflow
{
    public function __construct(private JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Delete subtask';
    }

    public function process(): bool
    {
        intro($this->info());


        return false;
    }
}