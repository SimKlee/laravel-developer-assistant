<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

use Illuminate\Support\Collection;

abstract class AbstractState
{
    protected Collection $workflow;

    public function __construct()
    {
        $this->workflow = new Collection();
    }

    abstract public function process(): bool;

    abstract public function check(): bool;

    abstract public function message(): string;

    abstract public function info(): string;
}