<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDto;

class JiraIssues
{
    private readonly JiraRequest $request;

    public function __construct(private ?string $dtoClass = null)
    {
        if (is_null($this->dtoClass)) {
            $this->dtoClass = JiraIssueDto::class;
        }

        $this->request  = new JiraRequest();
    }

    public function send(JqlBuilder|string $jql): Collection
    {
        $issues = new Collection();

        $response = $this->request->client()->get(
            sprintf('%s/search', $this->request->url),
            ['jql' => $jql instanceof JqlBuilder ? $jql->jsonString() : $jql]
        );

        if ($response->successful()) {
            $issues = collect($response->json('issues'))
                ->map(fn(array $issue) => new $this->dtoClass($issue));
        }

        return $issues;
    }
}