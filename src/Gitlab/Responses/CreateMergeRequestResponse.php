<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Gitlab\Responses;

use Carbon\Carbon;

class CreateMergeRequestResponse
{
    public readonly int     $id;
    public readonly int     $projectId;
    public readonly string  $title;
    public readonly ?string $description;
    public readonly ?string $state;
    public readonly string  $sourceBranch;
    public readonly string  $targetBranch;
    public readonly bool    $workInProgress;
    public readonly string  $mergeStatus;
    public readonly string  $webUrl;
    public readonly Carbon  $createdAt;
    public readonly Carbon  $updatedAt;

    public function __construct(array $data)
    {
        $this->id             = $data['id'];
        $this->projectId      = $data['project_id'];
        $this->title          = $data['title'];
        $this->description    = $data['description'];
        $this->state          = $data['state'];
        $this->sourceBranch   = $data['source_branch'];
        $this->targetBranch   = $data['target_branch'];
        $this->workInProgress = $data['work_in_progress'];
        $this->mergeStatus    = $data['merge_status'];
        $this->webUrl         = $data['web_url'];
        $this->createdAt      = Carbon::parse($data['created_at']);
        $this->updatedAt      = Carbon::parse($data['updated_at']);
    }
}