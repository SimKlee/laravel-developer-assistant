<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

/**
 * @property int    $id
 * @property string $type
 * @property string $key
 * @property string $status
 * @property string $summary
 * @property string $link
 */
class JiraSubtaskDto implements JiraIssueDtoInterface
{
    public readonly int     $id;
    public readonly string  $type;
    public readonly string  $key;
    public readonly string  $status;
    public readonly string  $summary;
    public readonly string  $link;

    public function __construct(array $issue)
    {
        $this->id       = (int) $issue['id'];
        $this->type     = $issue['fields']['issuetype']['name'];
        $this->key      = $issue['key'];
        $this->status   = $issue['fields']['status']['name'];
        $this->summary  = $issue['fields']['summary'];
        $this->link     = sprintf('%s/browse/%s', config('laravel-developer-assistant.jira.web_url', 'https://drwalter.atlassian.net'), $issue['key']);
    }

    public function toArray(): array
    {
        return [];
    }
}