<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\intro;
use function Laravel\Prompts\select;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\line;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\openUrlInBrowser;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\nextStatus;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\selectNextStatus;

class UserStoryWorkflow extends AbstractWorkflow
{
    public ?string $key = null;

    public function __construct(private JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Handle user story';
    }

    public function process(): bool
    {
        if ($this->issue->type !== 'User Story') {
            return false;
        }

        intro($this->info());
        assignIssue(issue: $this->issue, confirm: true);

        $actions = nextStatus($this->issue);
        $actions->put('commit', 'ACTION: Commit changes');
        $actions->put('open', 'ACTION: Open issue in Jira');
        $actions->put('comment', 'ACTION: Comment issue');
        $actions->put('skip', 'ACTION: Skip');

        switch (select(label: 'Select next status or action for ' . $this->issue->key, options: $actions, scroll: 15)) {
            case 'Done':
                (new CreateCodeReviewWorkflow($this->issue))->process();
                break;
            case 'comment':
                (new CommentJiraIssueWorkflow($this->issue))->process();
                break;
            case 'open':
                openUrlInBrowser($this->issue->link);
                break;
            case 'commit':
                (new CommitChangesWorkflow($this->issue))->process();
                break;
        }

        return false;
    }
}