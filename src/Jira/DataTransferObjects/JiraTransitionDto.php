<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

class JiraTransitionDto implements JiraTransitionDtoInterface
{
    public readonly int    $id;
    public readonly string $name;
    public readonly string $status;

    public function __construct(array $transition)
    {
        $this->id     = (int) $transition['id'];
        $this->name   = $transition['name'];
        $this->status = $transition['to']['name'];
    }

    public function toArray(): array
    {
        return [];
    }
}