<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Gitlab;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class GitlabRequest
{
    public readonly string  $url;
    public readonly string  $project;
    private readonly string $token;

    public function __construct()
    {
        $this->url     = config('laravel-development-assistant.gitlab.api_url');
        $this->token   = config('laravel-development-assistant.gitlab.token');
        $this->project = config('laravel-development-assistant.gitlab.project_id');
    }

    public function client(): PendingRequest
    {
        return Http::withHeaders([
            'Authorization' => sprintf('Bearer %s', $this->token),
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ])->withOptions([
            'connection_timeout' => 2,
            'timeout'            => 5,
        ]);
    }
}