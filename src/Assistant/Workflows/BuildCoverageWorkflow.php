<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Phpunit\Phpunit;
use function Laravel\Prompts\spin;

class BuildCoverageWorkflow extends AbstractWorkflow
{
    public function info(): string
    {
        return 'Workflow: Build coverage xml report';
    }

    public function process(): bool
    {
        spin(
            callback: fn() => (new Phpunit('vendor/bin'))
                ->coverageXml('build/coverage/xml')
                ->noOutput()
                ->throughSail()
                ->run(),
            message : $this->info()
        );

        return true;
    }
}