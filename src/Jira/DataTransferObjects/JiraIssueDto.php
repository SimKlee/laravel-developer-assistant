<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

/**
 * @property int    $id
 * @property string $type
 * @property string $key
 * @property string $project
 * @property string $projectId
 * @property string $status
 * @property string $summary
 * @property string $assignee
 * @property string $assigneeId
 * @property string $reporter
 * @property string $link
 */
class JiraIssueDto implements JiraIssueDtoInterface
{
    public const TYPE_BUG      = 'Bug';
    public const TYPE_TASK     = 'Task';
    public const TYPE_STORY    = 'Story';
    public const TYPE_SUBTASK  = 'Subtask';
    public const TYPE_SUB_TASK = 'Sub-task';

    public const STATUS_OPEN        = 'Open';
    public const STATUS_IN_PROGRESS = 'In Progress';
    public const STATUS_CODE_REVIEW = 'Code Review';
    public const STATUS_STAGED      = 'Staged';
    public const STATUS_LIVE        = 'Live';
    public const STATUS_DONE        = 'Done';

    public readonly int     $id;
    public readonly string  $type;
    public readonly string  $key;
    public readonly string  $project;
    public readonly int     $projectId;
    public readonly string  $status;
    public readonly string  $statusId;
    public readonly string  $summary;
    public readonly ?string $assignee;
    public readonly ?string $assigneeId;
    public readonly string  $reporter;
    public readonly string  $link;
    public readonly ?array  $subtasks;

    public function __construct(array $issue)
    {
        $this->id         = (int) $issue['id'];
        $this->type       = $issue['fields']['issuetype']['name'];
        $this->key        = $issue['key'];
        $this->project    = $issue['fields']['project']['name'];
        $this->projectId  = (int) $issue['fields']['project']['id'];
        $this->status     = $this->translateStatus($issue['fields']['status']['name']);
        $this->statusId   = $issue['fields']['status']['id'];
        $this->summary    = $issue['fields']['summary'];
        $this->assignee   = $issue['fields']['assignee']['displayName'] ?? null;
        $this->assigneeId = $issue['fields']['assignee']['accountId'] ?? null;
        $this->reporter   = $issue['fields']['reporter']['displayName'];
        $this->link       = sprintf(
            '%s/browse/%s',
            config('laravel-developer-assistant.jira.web_url', 'https://drwalter.atlassian.net'),
            $issue['key']
        );

        if ($issue['fields']['subtasks'] ?? false) {
            $this->subtasks = collect($issue['fields']['subtasks'])
                ->map(fn(array $subtask) => new JiraSubtaskDto($subtask))
                ->toArray();
        } else {
            $this->subtasks = [];
        }
    }

    private function translateStatus(string $status): string
    {
        return match ($status) {
            'Offen'       => self::STATUS_OPEN,
            'In Arbeit'   => self::STATUS_IN_PROGRESS,
            'Code Review' => self::STATUS_CODE_REVIEW,
            'Staged'      => self::STATUS_STAGED,
            'Live'        => self::STATUS_LIVE,
            'Fertig'      => self::STATUS_DONE,
            default       => $status
        };
    }

    public function toArray(): array
    {
        return [];
    }
}