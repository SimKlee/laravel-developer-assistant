<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\confirm;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\chooseIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\showIssues;

/**
 * @property Collection|JiraIssueDtoInterface[]
 */
class StartJiraIssueWorkflow extends AbstractWorkflow
{
    public function __construct(private readonly JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Start a new jira issue';
    }

    public function process(): bool
    {
        return false;
    }
}