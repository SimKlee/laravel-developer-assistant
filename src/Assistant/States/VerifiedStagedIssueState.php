<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class VerifiedStagedIssueState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['Staged'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            '"Verified[Dropdown]" = Yes',
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('assignee = %s', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking if any issues with status "Staged" are verified.';
    }
}