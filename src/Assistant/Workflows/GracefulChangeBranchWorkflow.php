<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

class GracefulChangeBranchWorkflow extends AbstractGitWorkflow
{

    public function info(): string
    {
        return 'Workflow: Change branch graceful';
    }

    public function process(): bool
    {
        (new CommitChangesWorkflow())->process();

        if ($this->git->isDevelopBranch() === false) {
            $this->git->checkout(config('laravel-developer-assistant.git.branches.develop'));
        }

        return false;
    }

    private function commitChanges()
    {

    }
}