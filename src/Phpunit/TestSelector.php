<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Git\GitStatusResult;

class TestSelector
{
    public Collection $tests;
    private Collection $ignore;

    public function __construct(private readonly GitStatusResult $status, string $pathToXmlCoverage, array $ignore = [])
    {
        $this->tests  = new Collection();
        $this->ignore = collect($ignore);
        $coverageMap  = new ClassCoverageMap($pathToXmlCoverage);

        $this->status->all()
                     ->each(function (string $file) use ($coverageMap) {
                         if ($this->ignore->has($file) === false && $coverageMap->map->has($file)) {
                             $coverageMap->map->get($file)
                                              ->tests()
                                              ->each(function (string $test) {
                                                  if ($this->tests->contains($test) === false) {
                                                      $this->tests->add($test);
                                                  }
                                              });
                         }
                     });
    }
}