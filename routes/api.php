<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;

Route::name('api.')
     ->prefix('api')
     ->group(function () {

         Route::get('/assistant', function () {
             $json = [
                 'code' => [
                     'loc' => 0,
                 ],
             ];

             return new JsonResponse($json);
         })->name('assistant');

     });
