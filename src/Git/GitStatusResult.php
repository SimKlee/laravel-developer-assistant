<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Git;

use Illuminate\Support\Collection;

class GitStatusResult
{
    public const FLAG_MODIFIED          = 'M';
    public const FLAG_FILE_TYPE_CHANGED = 'T';
    public const FLAG_ADDED             = 'A';
    public const FLAG_DELETED           = 'D';
    public const FLAG_RENAMED           = 'R';
    public const FLAG_COPIED            = 'C';
    public const FLAG_UNMERGED          = 'U';
    public const FLAG_UNTRACKED         = '?';
    public const FLAG_IGNORED           = '!';

    public function __construct(private readonly Collection $output)
    {
    }

    public function clean(): bool
    {
        return $this->all()->count() === 0;
    }

    public function dirty(): bool
    {
        return $this->all()->count() > 0;
    }

    public function all(): Collection
    {
        return $this->output->map(fn(string $file) => substr($file, 3));
    }

    public function modified(): Collection
    {
        return $this->analyseAndFilterByFlag([
            self::FLAG_MODIFIED,
            self::FLAG_FILE_TYPE_CHANGED,
            self::FLAG_RENAMED,
            self::FLAG_COPIED,
            self::FLAG_ADDED,
            self::FLAG_IGNORED,
        ]);
    }

    public function deleted(): Collection
    {
        return $this->analyseAndFilterByFlag([self::FLAG_DELETED]);
    }

    public function untracked(): Collection
    {
        return $this->analyseAndFilterByFlag([self::FLAG_UNTRACKED]);
    }

    public function unmerged(): Collection
    {
        return $this->analyseAndFilterByFlag([self::FLAG_UNMERGED]);
    }

    public function mergeConflicts(): bool
    {
        return $this->unmerged()->count() > 0;
    }

    private function analyseAndFilterByFlag(array $flags): Collection
    {
        return $this->output
            ->map(fn(string $line) => [
                'remote' => substr($line, 0, 1),
                'local'  => substr($line, 1, 1),
                'file'   => substr($line, 3),
            ])
            ->filter(fn(array $change) => in_array($change['remote'], $flags) || in_array($change['local'], $flags))
            ->map(fn(array $change) => $change['file']);
    }
}