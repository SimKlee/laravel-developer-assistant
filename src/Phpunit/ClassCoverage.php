<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit;

use Illuminate\Support\Collection;

class ClassCoverage
{
    private ?string    $fileName  = null;
    private ?string    $filePath  = null;
    private ?string    $className = null;
    private ?string    $namespace = null;
    private Collection $tests;

    public function __construct(private readonly string $file)
    {
        $data        = json_decode(json_encode(simplexml_load_file($this->file)), true);
        $this->tests = new Collection();

        $fileData = $data['file'] ?? false;

        if ($fileData) {
            $this->readFileInfo($fileData);
            $this->readClassInfo($fileData);
            $this->readTestsInfo($fileData);
        }
    }

    private function readFileInfo(array $data): void
    {
        $this->fileName = $data['@attributes']['name'] ?? null;
        $this->filePath = 'app' . $data['@attributes']['path'] ?? null;
    }

    private function readClassInfo(array $data): void
    {
        if ($data['class'] ?? false) {
            $this->className = last(explode('\\', $data['class']['@attributes']['name']));
            $this->namespace = $data['class']['namespace']['@attributes']['name'] ?? null;
        }
    }

    private function readTestsInfo(array $data): void
    {
        collect($data['coverage']['line'] ?? [])
            ->each(function (array $line) {
                collect($line['covered'] ?? [])->each(function (array $covered) {
                    if ($covered['@attributes']['by'] ?? false) {
                        $parts     = explode('::', $covered['@attributes']['by']);
                        $testClass = current($parts);
                        if ($this->tests->contains($testClass) === false) {
                            $this->tests->add($testClass);
                        }
                    }
                });
            });
    }

    public function fileName(bool $withPath = true): string|null
    {
        if ($withPath) {
            return $this->filePath . '/' . $this->fileName;
        }

        return $this->file;
    }

    public function className(bool $withNamespace = true): string|null
    {
        if ($withNamespace) {
            return $this->namespace . '\\' . $this->className;
        }

        return $this->className;
    }

    public function tests(): Collection
    {
        return $this->tests;
    }
}