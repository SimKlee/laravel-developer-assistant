<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Functions\Prompts;

use SimKlee\LaravelDeveloperAssistant\Prompts\PausePrompt;

function pause(): mixed
{
    return (new PausePrompt(...func_get_args()))->prompt();
}