<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\BuildCoverageWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\CommitChangesWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\CreateSubtaskWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\DeleteSubtaskWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\PintChangedFilesWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\RunNecessaryTestsWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\SubtaskWorkflow;
use SimKlee\LaravelDeveloperAssistant\Assistant\Workflows\UserStoryWorkflow;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDto;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use SimKlee\LaravelDeveloperAssistant\Phpunit\ClassCoverageMap;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\info;
use function Laravel\Prompts\select;
use function SimKlee\LaravelDeveloperAssistant\Functions\Base\when;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\clear;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssueByBranchName;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\selectNextStatus;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\showIssueWithSubtasks;

class WorkState extends AbstractJiraBasedState
{
    private ?JiraIssueDtoInterface $issue       = null;
    protected array                $issueStatus = ['In Progress'];
    protected array                $issueTypes  = [JiraIssueDto::TYPE_BUG, JiraIssueDto::TYPE_TASK, JiraIssueDto::TYPE_STORY];
    private bool                   $hideDone    = false;

    public function conditions(): string
    {
        return implode(' AND ', [
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('assignee = %s', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking if any of your issues are in progress.';
    }

    public function message(): string
    {
        return $this->issue instanceof JiraIssueDtoInterface
            ? 'Found the issue corresponding to your current branch: ' . $this->issue->key
            : 'Found no issue corresponding to your current branch';
    }

    public function check(): bool
    {
        $this->issue = getIssueByBranchName();
        if ($this->issue instanceof JiraIssueDto) {
            return true;
        }

        return parent::check();
    }

    public function process(): bool
    {
        $this->issue = getIssueByBranchName();

        intro('State: Work');
        if ($this->issue) {
            info('Found corresponding Jira issue ' . $this->issue->status);
            selectNextStatus($this->issue, ($this->issue->status !== 'In Progress'));
        }

        $key = $this->selectIssue();
        while ($key !== 'skip') {

            switch ($key) {
                case 'create-subtask':
                    (new CreateSubtaskWorkflow($this->issue))->process();
                    break;
                case 'delete-subtask':
                    (new DeleteSubtaskWorkflow($this->issue))->process();
                    break;
                case 'hide-done':
                    $this->hideDone = true;
                    break;
                case 'show-done':
                    $this->hideDone = false;
                    break;
                case 'commit':
                    (new CommitChangesWorkflow())->process();
                    break;
                case 'refresh-coverage':
                    (new BuildCoverageWorkflow())->process();
                    break;
                case 'necessary-tests':
                    (new RunNecessaryTestsWorkflow())->process();
                    break;
                case 'pint':
                    (new PintChangedFilesWorkflow())->process();
                    break;
            }

            $issue = getIssue($key);
            if ($issue) {
                switch ($issue->type) {
                    case 'Sub-task':
                        (new SubtaskWorkflow($issue))->process();

                        break;
                    case 'User Story':
                        (new UserStoryWorkflow($issue))->process();
                        break;
                }
            }

            clear();
            intro('State: Work');
            info($this->message());
            $key = $this->selectIssue();
        }

        return true;
    }

    private function selectIssue(): string
    {
        $issues = showIssueWithSubtasks(
            issue   : getIssue($this->issue->key),
            columns : ['type', 'key', 'status', 'summary:short', 'assignee:firstWord', 'reporter:firstWord', 'link'],
            hideDone: $this->hideDone
        );

        $options = $this->options($issues);

        // @TODO: make default value more dynamic and intelligent

        return select(label: 'Choose issue or action', options: $options, scroll: 30);
    }

    private function options(Collection $issues): Collection
    {
        $options = $issues->mapWithKeys(fn(JiraIssueDtoInterface $issue) => [
            $issue->key => sprintf('%s: [%s] %s %s', strtoupper($issue->type), $issue->status, $issue->key, $issue->summary),
        ]);

        $coverageCreation = ClassCoverageMap::indexCreation(base_path('build/coverage/xml'));
        $creation         = ($coverageCreation instanceof Carbon)
            ? $coverageCreation->locale('en_EN')->diffForHumans()
            : 'not found';

        $options->put('pint', 'CLEAN: Check code style (only changed files)')
                ->put('necessary-tests', 'TEST: Run necessary tests')
                ->put('refresh-coverage', sprintf('TEST: Refresh coverage report (last: %s)', $creation));

        when(
            condition: $this->hideDone,
            then     : fn() => $options->put('show-done', 'ACTION: Show done subtasks'),
            else     : fn() => $options->put('hide-done', 'ACTION: Hide done subtasks')
        );

        $options->put('create-subtask', 'ACTION: Create subtask')
                ->put('delete-subtask', 'ACTION: Delete subtask')
                ->put('commit', 'ACTION: Commit changes')
                ->put('refresh', 'ACTION: Refresh state')
                ->put('skip', 'ACTION: Skip state');

        return $options;
    }
}