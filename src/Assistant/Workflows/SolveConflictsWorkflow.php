<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use Illuminate\Support\Collection;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\error;
use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\select;
use function Laravel\Prompts\text;
use function SimKlee\LaravelDeveloperAssistant\Functions\Base\when;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\line;

class SolveConflictsWorkflow extends AbstractGitWorkflow
{
    private ?Collection $unsolved = null;

    public function info(): string
    {
        return 'Workflow: Solve merge conflicts';
    }

    public function process(): bool
    {
        $changes = $this->git->status();
        if ($changes->mergeConflicts()) {
            $unmerged = $changes->unmerged();
            error('You have merge conflicts after pulling changes from origin:');
            $unmerged->each(fn(string $file) => line(' - ' . $file));
            $choice = select(
                label  : 'Choice',
                options: [
                    'continue' => 'Wait for me. I will solve the conflicts immediately and continue.',
                    'abort'    => 'I want abort and continue later after merging conflicts.',
                ],
                default: 'continue'
            );
            if ($choice === 'abort') {
                return false;
            }

            if ($this->solveConflicts($unmerged) === false) {
                return false;
            }

            $this->git->push();
        }

        return true;
    }

    private function solveConflicts(Collection $unmerged): bool
    {
        $unsolved = $this->git->conflicts();

        while ($unsolved->count() > 0) {
            error('Unsolved conflicts:');
            $unsolved->each(fn(string $file) => line(' - ' . $file));
            $solved = $unmerged->reject(fn(string $file) => $unsolved->contains($file));

            if ($solved->count() > 0) {
                $files = collect(multiselect('Commit solved files:', $solved->prepend('ALL')->prepend('SKIP')));
                if ($files->contains('SKIP')) {
                    break;
                }

                if ($files->contains('ALL')) {
                    $files = $solved;
                    $this->git->add($files)
                              ->commit(text('Commit message'));
                }
            }

            if (confirm('Check again?')) {
                $unsolved = $this->git->conflicts();
            }
        }

        $this->git->pull()
                  ->push();

        return $unsolved->count() === 0;
    }

    /**
     * @deprecated
     */
    public function process2(): bool
    {
        // @TODO: use GitStatusResult::mergeConflicts() (as in CommitChangesWorkflow::commit)

        $this->unsolved = $this->git->conflicts();
        $unsolved       = $this->unsolved;

        if ($unsolved->count() === 0) {
            return true;
        }

        while ($this->unsolved->count() > 0) {
            error('Unsolved conflicts detected:');
            $unsolved->each(fn(string $file) => line(' - ' . $file));
            line('');

            $this->commitSolved($unsolved);
            when($this->git->notPushed(), fn() => $this->git->pull()->push());

            if (confirm('Check again') === false) {
                return false;
            }

            $unsolved = $this->git->conflicts();
        }

        return true;
    }

    /**
     * @deprecated
     */
    private function solved(Collection $unsolved): Collection
    {
        return $this->unsolved->reject(fn(string $file) => $unsolved->contains($file));
    }

    /**
     * @deprecated
     */
    private function commitSolved(Collection $unsolved): void
    {
        $solved = $this->solved($unsolved);
        if ($solved->count() === 0) {
            return;
        }

        $files = collect(multiselect('Commit solved files', $solved, $solved));
        if ($files->count() === 0) {
            return;
        }

        $this->git->add($files)
                  ->commit(text(label: 'Commit message', default: 'solved merge conflicts'));

        $this->unsolved = $this->unsolved->reject(fn(string $file) => $files->contains($file));
    }
}