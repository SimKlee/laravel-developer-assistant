<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssue;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\text;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\clear;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\createSubtask;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\getIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\selectNextStatus;

class CreateSubtaskWorkflow extends AbstractWorkflow
{
    public ?string $key = null;

    public function __construct(private JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Create subtask';
    }

    public function process(): bool
    {
        intro($this->info());
        $this->key = createSubtask($this->issue);

        return $this->key !== false;
    }
}