<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Gitlab;

use SimKlee\LaravelDeveloperAssistant\Gitlab\Responses\CreateMergeRequestResponse;

class CreateMergeRequest
{
    private readonly GitlabRequest $request;

    private ?string $sourceBranch;
    private ?string $targetBranch;
    private ?string $title;
    private ?string $description;

    public function __construct()
    {
        $this->request = new GitlabRequest();
    }

    public function create(): CreateMergeRequestResponse|false
    {
        if (is_null($this->title)) {
            $this->title = sprintf('Merge %s into %s', $this->sourceBranch, $this->targetBranch);
        }

        $url      = sprintf('%s/projects/%s/merge_requests', $this->request->url, $this->request->project);
        $response = $this->request->client()->post($url, $this->data());

        if ($response->successful()) {
            return new CreateMergeRequestResponse($response->json());
        }

        return false;
    }

    private function data(): array
    {
        $data = [
            'source_branch' => $this->sourceBranch,
            'target_branch' => $this->targetBranch,
            'title'         => $this->title,
        ];

        if (!is_null($this->description)) {
            $data['description'] = $this->description;
        }

        return $data;
    }

    public function sourceBranch(string $sourceBranch): self
    {
        $this->sourceBranch = $sourceBranch;

        return $this;
    }

    public function targetBranch(string $targetBranch): self
    {
        $this->targetBranch = $targetBranch;

        return $this;
    }

    public function title(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function description(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}