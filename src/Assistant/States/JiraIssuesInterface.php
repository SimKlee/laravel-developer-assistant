<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

use Illuminate\Support\Collection;

interface JiraIssuesInterface
{
    public function issues(): Collection;
}