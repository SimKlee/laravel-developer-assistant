<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\text;

/**
 * @property Collection|JiraIssueDtoInterface[]
 */
class CommentJiraIssueWorkflow extends AbstractWorkflow
{
    public function __construct(private readonly JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Comment jira issue';
    }

    public function process(): bool
    {
        text(label: 'Add comment to ' . $this->issue->key, required: true);

        return false;
    }
}