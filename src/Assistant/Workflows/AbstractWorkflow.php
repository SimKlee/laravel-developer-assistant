<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

abstract class AbstractWorkflow
{
    abstract public function process(): bool;

    abstract public function info(): string;
}