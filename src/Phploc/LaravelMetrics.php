<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phploc;

use Illuminate\Support\Facades\File;

class LaravelMetrics
{
    public readonly int $command;
    public readonly int $controller;
    public readonly int $middleware;
    public readonly int $event;
    public readonly int $listener;
    public readonly int $job;
    public readonly int $blade;
    public readonly int $action;
    public readonly int $model;

    public function __construct()
    {
        $this->analyse();
    }

    private function analyse(): void
    {
        $this->command    = $this->countFilesInDirectory(app_path('Console/Commands'));
        $this->controller = $this->countFilesInDirectory(app_path('Http/Controllers'));
        $this->middleware = $this->countFilesInDirectory(app_path('Http/Middleware'));
        $this->event      = $this->countFilesInDirectory(app_path('Events'));
        $this->listener   = $this->countFilesInDirectory(app_path('Listeners'));
        $this->job        = $this->countFilesInDirectory(app_path('Jobs'));
        $this->action     = $this->countFilesInDirectory(app_path('Actions'));
        $this->blade      = $this->countFilesInDirectory(resource_path('views'));
        $this->model      = $this->countFilesInDirectory(app_path('Models'), recursive: false);
    }

    private function countFilesInDirectory(string $directory, bool $recursive = true): int
    {
        if (File::isDirectory($directory) === false) {
            return 0;
        }

        $count = count(File::files($directory));

        if ($recursive) {
            $count += collect(File::directories($directory))
                ->map(fn(string $dir) => $this->countFilesInDirectory($dir))
                ->sum();
        }

        return $count;
    }
}