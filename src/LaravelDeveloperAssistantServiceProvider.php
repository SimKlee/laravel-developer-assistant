<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant;

use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelDeveloperAssistant\Console\Commands\AssistantCommand;

class LaravelDeveloperAssistantServiceProvider extends ServiceProvider
{
    public function register(): void
    {

    }

    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/laravel-developer-assistant.php', 'laravel-developer-assistant');

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'laravel-developer-assistant');

        $this->commands([
            AssistantCommand::class,
        ]);
    }
}
