<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phploc;

class PhplocResult
{
    /** @var int Directories */
    public readonly int $directories;
    /** @var int Files */
    public readonly int $files;
    /** @var int Lines of code */
    public readonly int $loc;
    /** @var int Logical Lines of Code */
    public readonly int $lloc;
    /** @var int Logical Lines of Code Classes */
    public readonly int $llocClasses;
    /** @var int Logical Lines of Functions */
    public readonly int $llocFunctions;
    /** @var int Not in classes or functions */
    public readonly int $llocGlobal;
    /** @var int Comment Lines of Code */
    public readonly int $cloc;
    /** @var int */
    public readonly int $ccn;
    /** @var int Methods */
    public readonly int $ccnMethods;
    /** @var int Interfaces */
    public readonly int $interfaces;
    /** @var int Traits */
    public readonly int $traits;
    /** @var int Classes */
    public readonly int $classes;
    /** @var int Abstract Classes */
    public readonly int $abstractClasses;
    /** @var int Concrete Classes */
    public readonly int $concreteClasses;
    /** @var int Final Classes */
    public readonly int $finalClasses;
    /** @var int Non-Final Classes */
    public readonly int $nonFinalClasses;
    /** @var int Functions */
    public readonly int $functions;
    /** @var int Named Functions */
    public readonly int $namedFunctions;
    /** @var int Anonymous Functions */
    public readonly int $anonymousFunctions;
    /** @var int Methods */
    public readonly int $methods;
    /** @var int Public Methods */
    public readonly int $publicMethods;
    /** @var int Non-Public Methods */
    public readonly int $nonPublicMethods;
    /** @var int Protected Methods */
    public readonly int $protectedMethods;
    /** @var int Private Methods */
    public readonly int $privateMethods;
    /** @var int Non-Static Methods */
    public readonly int $nonStaticMethods;
    /** @var int Static Methods */
    public readonly int $staticMethods;
    /** @var int Constants */
    public readonly int $constants;
    /** @var int Class Constants */
    public readonly int $classConstants;
    /** @var int Public Class Constants */
    public readonly int $publicClassConstants;
    /** @var int Non-Public Class Constants */
    public readonly int $nonPublicClassConstants;
    /** @var int Global Constants */
    public readonly int $globalConstants;
    /** @var int Test Classes */
    public readonly int $testClasses;
    /** @var int Test Methods */
    public readonly int $testMethods;
    /** @var float */
    public readonly float $ccnByLloc;
    public readonly float $llocByNof;
    public readonly int   $methodCalls;
    public readonly int   $staticMethodCalls;
    public readonly int   $instanceMethodCalls;
    public readonly int   $attributeAccesses;
    public readonly int   $staticAttributeAccesses;
    public readonly int   $instanceAttributeAccesses;
    public readonly int   $globalAccesses;
    public readonly int   $globalVariableAccesses;
    public readonly int   $superGlobalVariableAccesses;
    public readonly int   $globalConstantAccesses;
    public readonly int   $classCcnMin;
    public readonly float $classCcnAvg;
    public readonly int   $classCcnMax;
    public readonly int   $classLlocMin;
    public readonly float $classLlocAvg;
    public readonly int   $classLlocMax;
    public readonly int   $methodCcnMin;
    public readonly float $methodCcnAvg;
    public readonly int   $methodCcnMax;
    public readonly int   $methodLlocMin;
    public readonly float $methodLlocAvg;
    public readonly int   $methodLlocMax;
    public readonly float $averageMethodsPerClass;
    public readonly int   $minimumMethodsPerClass;
    public readonly int   $maximumMethodsPerClass;
    public readonly int   $namespaces;
    public readonly int   $ncloc;

    public function __construct(private readonly string $resultJsonFile = 'phploc.json')
    {
        $this->fromArray(json_decode(file_get_contents($this->resultJsonFile), true));
    }

    private function fromArray(array $data): void
    {
        collect($data)->each(fn($value, string $key) => $this->{$key} = $value);
    }
}