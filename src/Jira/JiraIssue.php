<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDto;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraTransitionDto;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraTransitionDtoInterface;

class JiraIssue
{
    private readonly JiraRequest $request;

    public function __construct(private ?string $dtoClass = null)
    {
        if (is_null($this->dtoClass)) {
            $this->dtoClass = JiraIssueDto::class;
        }

        $this->request = new JiraRequest();
    }

    public function getByBranchName(string $branch): JiraIssueDtoInterface|false
    {
        $parts = explode('-', $branch);
        if (count($parts) >= 2) {
            $key = sprintf('%s-%s', $parts[0], $parts[1]);

            return $this->get($key);
        }

        return false;
    }

    public function get(string $key): JiraIssueDtoInterface|false
    {
        $response = $this->request->client()->get(sprintf('%s/issue/%s', $this->request->url, $key));

        if ($response->successful()) {
            #dump($response->json());
            return new $this->dtoClass($response->json());
        }

        return false;
    }

    public function assign(string $key, string $userId = null): bool
    {
        $userId   = $userId ?? config('laravel-developer-assistant.jira.user_id');
        $response = $this->request->client()->put(
            url : sprintf('%s/issue/%s/assignee', $this->request->url, $key),
            data: ['accountId' => $userId]
        );

        return $response->successful();
    }

    public function unassign(string $key): bool
    {
        $response = $this->request->client()->put(
            url : sprintf('%s/issue/%s/assignee', $this->request->url, $key),
            data: ['accountId' => null]
        );

        return $response->successful();
    }

    /**
     * @return Collection|JiraTransitionDtoInterface[]
     */
    public function nextStatus(string $key, bool $asKeyValue = false): Collection
    {
        $transitions = new Collection();
        $path        = sprintf('/issue/%s/transitions', $key);
        $response    = $this->request->client()->get($this->request->url . $path);

        if ($response && $response->successful()) {
            $json        = $response->json();
            $transitions = collect($json['transitions'])->map(fn(array $issue) => new JiraTransitionDto($issue));
        }

        if ($asKeyValue) {
            return $transitions->mapWithKeys(
                fn(JiraTransitionDto $transitionDto) => [$transitionDto->id => 'STATUS: ' . $transitionDto->status]
            );
        }

        return $transitions;
    }

    public function setStatus(string $key, string|int $transition): bool
    {
        $path     = sprintf('/issue/%s/transitions', $key);
        $response = $this->request->client()->post($this->request->url . $path, [
            'transition' => [
                'id' => $transition,
            ],
        ]);

        return $response->successful();
    }


    public function createSubtask(JiraIssueDtoInterface $issue, string $summary, string $userId = null): string|false
    {
        $data = [
            'fields' => [
                'project'   => ['id' => $issue->projectId],
                'issuetype' => ['name' => 'Sub-task'],
                'parent'    => ['key' => $issue->key],
                'summary'   => $summary,
            ],
        ];
        if (!is_null($userId)) {
            $data['fields']['assignee'] = ['id' => $userId];
        }

        $response = $this->request->client()->post($this->request->url . '/issue', $data);

        if ($response->failed()) {
            return false;
        }

        return $response->json('key');
    }

    public function issueTypes(): Collection
    {
        $response = $this->request->client()->get($this->request->url . '/issuetype');

        return $response->successful();
    }
}