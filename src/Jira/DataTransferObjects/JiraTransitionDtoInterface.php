<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

/**
 * @property int    $id
 * @property string $name
 * @property string $status
 */
interface JiraTransitionDtoInterface
{
    public function toArray(): array;
}