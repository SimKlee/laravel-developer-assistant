<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\AbstractState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\EmergencyState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\FalsifiedCodeReviewIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\FalsifiedLiveIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\FalsifiedStagedIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\InProgressState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\OpenNotAssignedState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\OpenState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\VerifiableCodeReviewIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\VerifiedCodeReviewIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\VerifiedStagedIssueState;
use SimKlee\LaravelDeveloperAssistant\Assistant\States\WorkState;

use SimKlee\LaravelDeveloperAssistant\Git\Git;
use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssue;
use SimKlee\LaravelDeveloperAssistant\Phpunit\ClassCoverageMap;
use SimKlee\LaravelDeveloperAssistant\Phpunit\TestSelector;
use function Laravel\Prompts\error;
use function Laravel\Prompts\info;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\clear;

class AssistantCommand extends Command
{
    protected $signature   = 'assistant {--work} {--test}';
    protected $description = 'Your friendly developer assistant.';

    public function handle(): void
    {
        if ($this->option('test')) {
            $this->handleTest();
            return;
        }

        if ($this->option('work')) {
            $this->handleWork();
            return;
        }

        clear();
        collect([
            EmergencyState::class,
            FalsifiedLiveIssueState::class,
            FalsifiedStagedIssueState::class,
            FalsifiedCodeReviewIssueState::class,
            VerifiableCodeReviewIssueState::class,
            VerifiedStagedIssueState::class,
            VerifiedCodeReviewIssueState::class,
            InProgressState::class,
            OpenState::class,
            OpenNotAssignedState::class,
        ])->each(function (string $state) {
            /** @var AbstractState $state */
            $state = new $state();
            info($state->info());
            if ($state->check()) {
                error($state->message());
                $state->process();
            }
        });
    }

    public function handleWork(): void
    {
        clear();
        collect([
            WorkState::class,
            OpenState::class,
            OpenNotAssignedState::class,
        ])->each(function (string $state) {
            /** @var AbstractState $state */
            $state = new $state();
            info($state->info());
            if ($state->check()) {
                error($state->message());
                $state->process();
            }
        });
    }

    private function handleTest(): void
    {
        clear();
        $state = new WorkState();
        if ($state->check()) {
            $state->process();
        } else {
            error($state->message());
        }
    }
}