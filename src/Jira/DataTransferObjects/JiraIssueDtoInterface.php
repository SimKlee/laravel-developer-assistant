<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

/**
 * @property int    $id
 * @property string $type
 * @property string $key
 * @property string $project
 * @property string $projectId
 * @property string $status
 * @property string $summary
 * @property string $assignee
 * @property string $assigneeId
 * @property string $reporter
 * @property string $link
 * @property array  $subtasks
 */
interface JiraIssueDtoInterface
{
    public function toArray(): array;
}