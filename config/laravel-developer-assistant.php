<?php
return [
    'git'    => [
        'branches' => [
            'develop' => env('GIT_BRANCH_DEVELOP', 'develop'),
            'main'    => env('GIT_BRANCH_MAIN', 'main'),
        ],
    ],
    'gitlab' => [
        'api_url'    => env('GITLAB_API_URL', 'https://gitlab.com/api/v4'),
        'token'      => env('GITLAB_TOKEN'),
        'project_id' => env('GITLAB_PROJECT_ID'),
    ],
    'jira'   => [
        'web_url'    => env('JIRA_WEB_URL'),
        'api_url'    => env('JIRA_API_URL', 'https://gitlab.com/api/v4'),
        'user_id'    => env('JIRA_USER_ID'),
        'user_email' => env('JIRA_USER_EMAIL'),
        'token'      => env('JIRA_TOKEN'),
        'project'    => env('JIRA_PROJECT'),
        'cache_ttl'  => env('JIRA_CACHE_TTL'),
        'jql'        => [
            'named_filter' => [

            ],
        ],
    ],
];