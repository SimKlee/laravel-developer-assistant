<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;

use function Laravel\Prompts\confirm;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\assignIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\chooseIssue;
use function SimKlee\LaravelDeveloperAssistant\Functions\Jira\showIssues;

/**
 * @property Collection|JiraIssueDtoInterface[]
 */
class NextJiraIssueWorkflow extends AbstractWorkflow
{
    public ?JiraIssueDtoInterface $issue;

    public function __construct(private readonly Collection $issues)
    {
    }

    public function info(): string
    {
        return 'Workflow: Choose next jira issue';
    }

    public function process(): bool
    {
        showIssues($this->issues);
        $issue = chooseIssue($this->issues, skipOption: true);
        if ($issue && confirm('Assign issue?')) {
            $this->issue = $issue;
            if (assignIssue($issue)) {
                return true;
            }
        }

        return false;
    }
}