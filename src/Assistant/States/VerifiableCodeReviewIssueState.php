<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class VerifiableCodeReviewIssueState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['Code Review'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            '"Verified[Dropdown]" = EMPTY',
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            sprintf('assignee != %s', config('laravel-developer-assistant.jira.user_id')),
        ]);
    }

    public function info(): string
    {
        return 'Checking for open code reviews.';
    }
}