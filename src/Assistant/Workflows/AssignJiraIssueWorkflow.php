<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;
use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssue;

/**
 * @property Collection|JiraIssueDtoInterface[]
 */
class AssignJiraIssueWorkflow extends AbstractWorkflow
{
    public function __construct(private readonly JiraIssueDtoInterface $issue)
    {
    }

    public function info(): string
    {
        return 'Workflow: Assign a jira issue when unassigned';
    }

    public function process(): bool
    {
        if ($this->issue->assigneeId === config('laravel-developer-assistant.jira.user_id')) {
            return true;
        }

        return (new JiraIssue())->assign($this->issue->key);
    }
}