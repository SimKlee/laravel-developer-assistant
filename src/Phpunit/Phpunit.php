<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Phpunit;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Console\Command;

class Phpunit
{
    private Collection $output;
    private Collection $options;
    private Collection $files;
    private ?string    $configuration = null;
    private bool       $sail          = false;

    public function __construct(private readonly string $pathToPhpunit = 'vendor/bin')
    {
        $this->output  = new Collection();
        $this->options = new Collection();
        $this->files   = new Collection();
    }

    public function files(Collection|array|string $files): self
    {
        if (is_string($files)) {
            $files = collect([$files]);
        }

        if (is_array($files)) {
            $files = collect($files);
        }

        if ($files instanceof Collection) {
            $files->each(fn(string $file) => $this->files->add($file));
        }

        return $this;
    }

    public function configuration(string $configuration): self
    {
        $this->configuration = $configuration;

        return $this;
    }

    public function throughSail(): self
    {
        $this->sail = true;

        return $this;
    }

    public function noOutput(): self
    {
        $this->options->put('--no-output', true);

        return $this;
    }

    public function noProgress(): self
    {
        $this->options->put('--no-progress', true);

        return $this;
    }

    public function noResults(): self
    {
        $this->options->put('--no-results', true);

        return $this;
    }

    public function coverageXml(string $path): self
    {
        $this->options->put('--coverage-xml', $path);

        return $this;
    }

    public function coverageHtml(string $path): self
    {
        $this->options->put('--coverage-html', $path);

        return $this;
    }

    public function run(): void
    {
        $this->output = new Collection();
        $command      = new Command(
            command         : $this->buildCommand(),
            workingDirectory: base_path(),
            options         : [
                Command::OPTION_FILTER_EMPTY_OUTPUT_LINES => true,
                Command::OPTION_TRIM_OUTPUT_LINES         => true,
                Command::OPTION_TIMEOUT                   => 300,
            ]
        );
        $command->execute();
        $this->output = $command->output();

        dd($this->output);
    }

    private function buildCommand(): string
    {
        $command = $this->pathToPhpunit;
        $command .= $this->sail ? '/sail test' : '/phpunit';
        if ($this->options->count() > 0) {
            $command .= ' ' . $this->options->map(
                    fn($value, string $option) => ($value === true) ? $option : sprintf('%s %s', $option, $value)
                )->implode(' ');
        }

        if ($this->files->count() > 0) {
            $command .= ' ' . $this->files->implode(' ');
        }

        if ($this->configuration) {
            $command .= ' --configuration ' . $this->configuration;
        }
dump($command);
        return $command;
    }

    public function output(): Collection
    {
        return $this->output;
    }
}