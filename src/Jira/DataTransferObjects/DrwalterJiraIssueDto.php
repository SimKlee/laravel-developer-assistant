<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects;

/**
 * @property int    $id
 * @property string $type
 * @property string $key
 * @property string $project
 * @property string $status
 * @property string $summary
 * @property string $assignee
 * @property string $reporter
 * @property string $link
 * @property array  $developers
 * @property bool   $verified
 */
class DrwalterJiraIssueDto extends JiraIssueDto
{
    public readonly ?array  $developers;
    public readonly ?bool   $verified;

    public function __construct(array $issue)
    {
        parent::__construct($issue);

        if ($issue['fields']['customfield_10030'] ?? false) {
            $this->developers = collect($issue['fields']['customfield_10030'])
                ->map(fn(array $user) => $user['displayName'])
                ->toArray();
        } else {
            $this->developers = [];
        }

        if ($issue['fields']['customfield_10029'] ?? false) {
            $this->verified = match ($issue['fields']['customfield_10029']['value']) {
                'Yes'   => true,
                'No'    => false,
                default => null,
            };
        } else {
            $this->verified = null;
        }
    }
}