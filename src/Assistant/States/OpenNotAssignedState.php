<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\States;

class OpenNotAssignedState extends AbstractJiraBasedState
{
    protected array $issueStatus = ['Open'];
    protected array $issueTypes  = [];

    public function conditions(): string
    {
        return implode(' AND ', [
            'type NOT IN (Subtask, Sub-task)',
            'Sprint IN openSprints()',
            'Sprint NOT IN futureSprints()',
            'assignee IS EMPTY',
        ]);
    }

    public function info(): string
    {
        return 'Checking if any of your issues are open and not assigned.';
    }
}