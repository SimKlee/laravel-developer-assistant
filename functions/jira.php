<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Functions\Jira;

use Illuminate\Support\Collection;
use SimKlee\LaravelDeveloperAssistant\Git\Git;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraIssueDtoInterface;
use SimKlee\LaravelDeveloperAssistant\Jira\DataTransferObjects\JiraSubtaskDto;
use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssue;
use SimKlee\LaravelDeveloperAssistant\Jira\JiraIssues;

use function Laravel\Prompts\confirm;
use function Laravel\Prompts\select;
use function Laravel\Prompts\table;
use function Laravel\Prompts\text;
use function SimKlee\LaravelDeveloperAssistant\Functions\Console\openUrlInBrowser;

function showIssues(Collection $issues, array $columns = null): void
{
    $columns = $columns ?? ['type', 'key', 'status', 'assignee', 'summary', 'reporter', 'link'];
    $issues  = $issues->map(
        function (JiraIssueDtoInterface $issue) use ($columns) {
            return collect($columns)->map(
                fn(string $column) => castValue($issue, $column)
            )->toArray();
        }
    )->toArray();

    $headers = collect($columns)
        ->map(function (string $column) {
            if (str_contains($column, ':')) {
                return ucfirst(current(explode(':', $column)));
            }

            return ucfirst($column);
        });

    table(headers: $headers, rows: $issues);
}

function castValue(JiraIssueDtoInterface $issue, string $column): mixed
{
    $func = null;
    if (str_contains($column, ':')) {
        [$column, $func] = explode(':', $column);
    }

    $value = $issue->{$column};

    if (!is_null($func)) {
        $value = match ($func) {
            'short'     => short($value),
            'firstWord' => firstWord($value),
        };
    }

    if (is_bool($value)) {
        $value = $value === true ? 'Yes' : 'No';
    }

    return $value;
}

function showIssueWithSubtasks(JiraIssueDtoInterface $issue, array $columns = null, int $maxSummary = 20, bool $hideDone = false): Collection
{
    $columns = $columns ?? ['type', 'key', 'status', 'assignee', 'summary', 'reporter', 'link'];
    $jql     = sprintf('key IN (%s)', collect($issue->subtasks)->map(fn(JiraSubtaskDto $subtask) => $subtask->key)->implode(', '));
    if ($hideDone) {
        $jql .= ' AND status != Done';
    }
    $issues = (new JiraIssues())->send($jql . ' ORDER BY rank');
    $issues->prepend($issue);

    showIssues($issues, $columns, $maxSummary);

    return $issues;
}

function chooseIssue(Collection $issues, bool $skipOption = false): JiraIssueDtoInterface|null
{
    $options = $issues->mapWithKeys(fn(JiraIssueDtoInterface $issue) => [$issue->key => sprintf('[%s] %s', $issue->key, $issue->summary)]);
    if ($skipOption) {
        $options->put('skip', 'SKIP');
    }

    $key = select('Select jira issue', $options);
    if ($key === 'skip') {
        return null;
    }

    return (new JiraIssue())->get($key);
}

function assignIssue(JiraIssueDtoInterface $issue, string $userId = null, bool $confirm = false): bool
{
    $userId = $userId ?? config('laravel-developer-assistant.jira.user_id');

    if ($issue->assigneeId === $userId) {
        return false;
    }

    if ($confirm === false || confirm(sprintf('Assign issue %s?', $issue->key))) {
        return (new JiraIssue())->assign($issue->key, $userId);
    }

    return false;
}

function selectNextStatus(JiraIssueDtoInterface $issue, bool $condition = true): bool
{
    if ($condition) {
        $jiraRequest = new JiraIssue();
        $options     = $jiraRequest->nextStatus($issue->key, asKeyValue: true);
        // @TODO: set default dynamic
        $options->put('open', 'ACTION: Open issue in Jira')
                ->put('skip', 'ACTION: Skip');
        $nextStatus = select(label: 'Select status for ' . $issue->key, options: $options);

        if ($nextStatus === 'open') {
            openUrlInBrowser($issue->link);
        }

        if ($nextStatus !== 'skip') {
            return $jiraRequest->setStatus($issue->key, $nextStatus);
        }
    }

    return false;
}

function nextStatus(JiraIssueDtoInterface $issue): Collection
{
    return (new JiraIssue())->nextStatus(key: $issue->key, asKeyValue: true);
}

function getIssue(string $key): JiraIssueDtoInterface|false
{
    return (new JiraIssue())->get($key);
}

function getIssueByBranchName(string $branch = null): JiraIssueDtoInterface|false
{
    $branch = $branch ?? (new Git())->currentBranch();

    return (new JiraIssue())->getByBranchName($branch);
}

function createSubtask(JiraIssueDtoInterface $issue): string
{
    return (new JiraIssue())->createSubtask($issue, text(label: 'Summary', required: true));
}

function short(string $string, int $maxLength = 50): string
{
    if (mb_strlen($string) > $maxLength) {
        return mb_substr($string, 0, $maxLength) . ' ...';
    }

    return $string;
}

function firstWord(mixed $string): mixed
{
    if (is_string($string)) {
        return current(explode(' ', $string));
    }

    return $string;
}