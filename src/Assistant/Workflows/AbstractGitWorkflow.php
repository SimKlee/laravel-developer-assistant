<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Assistant\Workflows;

use SimKlee\LaravelDeveloperAssistant\Git\Git;

abstract class AbstractGitWorkflow extends AbstractWorkflow
{
    protected readonly Git $git;

    public function __construct()
    {
        $this->git = new Git();
    }
}