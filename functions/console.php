<?php

declare(strict_types=1);

namespace SimKlee\LaravelDeveloperAssistant\Functions\Console;

use Illuminate\Support\Facades\Process;

function line(string $text): void
{
    echo " {$text}\n";
}

function clear(): void
{
    echo "\033c";
}

function openUrlInBrowser(string $url): void
{
    Process::run('xdg-open ' . $url);
}